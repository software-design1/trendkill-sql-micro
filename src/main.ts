/**
 * Copyright (C) 2021  Raffaele Francesco Mancino raffaelefrancesco.mancino@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 **/
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

import { config } from 'dotenv';

async function bootstrap() {
  config({
    override: false
  });

  const version = process.env.npm_package_version;
  // deanMl();
  name(version);

  const app = await NestFactory.create(AppModule);
  swagger(app, version);
  await app.listen(3000);
}

function name(version) {
  console.log(
    '___________                          ._____   .__.__  .__   \n' +
      '\\__    ___/______   ____   ____    __| _/  | _|__|  | |  |  \n' +
      '  |    |  \\_  __ \\_/ __ \\ /    \\  / __ ||  |/ /  |  | |  |  \n' +
      '  |    |   |  | \\/\\  ___/|   |  \\/ /_/ ||    <|  |  |_|  |__\n' +
      '  |____|   |__|    \\___  >___|  /\\____ ||__|_ \\__|____/____/\n' +
      '                       \\/     \\/      \\/     \\/             ' +
      ` v${version} - An agnostic RESTful Microservice, for SQL DB\n`,
  );
}

function deanMl() {
  const guitar =
    '  .-.                                                                                               \n' +
    ' smmmds+/-                                                                                          \n' +
    ' +mMMMMMmddy+/-`                                                                                    \n' +
    '  `:yNMMMMMMMmddyo/:`                                                                               \n' +
    '     `+hMMMMMMMMMNNdhyo::.                                                                          \n' +
    '       `-sNMMMMMMMMMMMNNdhyo/:.       `.--`                                                         \n' +
    '          -/yNMMMMMMMMMMMMMNNdhysoosyhhdmmy.                                         ` `:..s.`..:/` \n' +
    '             .+dMMMMMMMMMMMMMMMMNNNMMMMMMMmo.                                       `h+-hsohhyhmy:  \n' +
    '               `-hMMMMMMMMMMMMmyNMMMMMMNdhNdsoo+:.````````````````````           `./shmmmNMd+//.    \n' +
    '                 `NMMMMMNNNNNNy`dNNNNNNd+.hhshohosy/yyy/syss/+ys:/ys/:oss/:osso:/ydmNNNMMMms:`      \n' +
    '                .oMMMMMMMMMMMNh`dNNNNNNdo`hdshohosy/yyy/syss:+ys-/ys/-oss/-osso--hdmMMMMMMNy::.`    \n' +
    '             `:smMMMMMMMMMMMMMNyNMMMMMMNmhNm/.......```````````````````````````````.smydmhNmyyhh+.  \n' +
    '           ./hNMMMMMMMMMMMMMMMMMMMMMMMMMMMMm+`                                      `:`-/ //``..-.  \n' +
    '        `-omMMMMMMMMMNMMMMMMMNNmNNMMMMMMMMMMmy+`                                                    \n' +
    '      `:yNMMMMMMMMNMMMMNmdy+/-```-/shmNMMMMMMMmho.                                                  \n' +
    '    .+dNMMMMMMMMNNmhs+:.`             .:oymNNMMMmdo`                                                \n' +
    '   omMMMMMMNdyo/-`                         `:+sdNMm-                                                \n' +
    '   hNmhs+:.                                      ..                                                 \n' +
    '                                                                                                    \n';
  console.log(guitar);
}

function swagger(app, version: string): void {
  const config = new DocumentBuilder()
    .setTitle('Trendkill')
    .setDescription('An agnostic RESTful Microservice, for SQL DB')
    .setVersion(version)
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);
}

bootstrap();
