/**
 * Copyright (C) 2021  Raffaele Francesco Mancino raffaelefrancesco.mancino@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 **/
import { ConsoleLogger, Module } from '@nestjs/common';
import { KnexModule } from 'nestjs-knex';
import { AppController } from './app.controller';
import { SqlModule } from './sql/sql.module';

@Module({
  imports: [
    SqlModule,
    KnexModule.forRootAsync({
      useFactory: () => ({
        config: {
          client: process.env['DB_TYPE'],
          connection: {
            host: process.env['DB_HOST'],
            port: Number(process.env['DB_PORT']),
            user: process.env['DB_USERNAME'],
            password: process.env['DB_PASSWORD'],
            database: process.env['DB_DATABASE']
          },
        },
      }),
    }),
  ],
  controllers: [AppController],
  providers: [ConsoleLogger],
})
export class AppModule {}
