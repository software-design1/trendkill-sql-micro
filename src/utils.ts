/**
 * Copyright (C) 2021  Raffaele Francesco Mancino raffaelefrancesco.mancino@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 **/
import { Knex } from 'nestjs-knex';
import { Knex as knexJs } from 'knex';
import { ConsoleLogger, HttpException, HttpStatus } from '@nestjs/common';
import {
  DBScanTableRelation,
  OData,
  ODataFilter,
  ODataFilterType,
  ODataFilterTypeLiteral,
  ODataFilterTypeProperty,
  QueryParams,
} from './models';
import { DbScannerService } from './sql/db-scanner.service';
import QueryBuilder = knexJs.QueryBuilder;

export class TrendkillQueryBuilder {
  private expand: string[] = [];

  private _knexQuery: QueryBuilder;

  private get query(): QueryBuilder {
    return this._knexQuery;
  }

  private set query(value: QueryBuilder) {
    this._knexQuery = value;
  }

  constructor(
    private table: string,
    private knex: Knex,
    private logger: ConsoleLogger,
    private scanner: DbScannerService,
  ) {}

  getData(odata?: OData): void {
    this.query = this.knex.table(this.table);
    if (odata?.$select) {
      this.generateSelect(odata);
    }
    if (odata?.$filter) {
      this.generateFilter(odata);
    }
    if (odata?.$orderby) {
      this.generateOrdering(odata);
    }
    if (odata?.$expand) {
      this.generateExpand(odata);
    }
  }

  private generateSelect(odata: OData): void {
    odata.$select.forEach((value) => {
      this.query.select(value);
    });
    this.expand = odata.$select ?? [];
  }

  private generateFilter(odata: OData): void {
    this.buildFilter(odata.$filter);
  }

  private buildFilter(filter: ODataFilter): void {
    const values = TrendkillQueryBuilder.getValue(filter);
    switch (filter.type) {
      case ODataFilterType.EQ:
        this.query.where(values.left, values.right);
        break;
      case ODataFilterType.NOT_EQ:
        this.query.whereNot(values.left, values.right);
        break;
      case ODataFilterType.LESS:
        this.query.where(values.left, '<', values.right);
        break;
      case ODataFilterType.GREATER:
        this.query.where(values.left, '>', values.right);
        break;
      case ODataFilterType.LE_EQ:
        this.query.where(values.left, '<=', values.right);
        break;
      case ODataFilterType.GR_EQ:
        this.query.where(values.left, '>=', values.right);
        break;
      case ODataFilterType.AND:
        this.buildFilter(filter.left as ODataFilter);
        this.query.and;
        this.buildFilter(filter.right as ODataFilter);
        break;
      case ODataFilterType.OR:
        this.buildFilter(filter.left as ODataFilter);
        this.query.or;
        this.buildFilter(filter.right as ODataFilter);
        break;
      default:
        throw new Error('OData name not supported.');
    }
  }

  private static getValue(filter: ODataFilter): QueryParams {
    const ret: QueryParams = {
      left: '',
      right: '',
    };
    if (filter.left.type === ODataFilterType.LITERAL) {
      ret.left = (filter.left as ODataFilterTypeLiteral).value;
    } else if (filter.left.type === ODataFilterType.PROPERTY) {
      ret.left = (filter.left as ODataFilterTypeProperty).name;
    }
    if (filter.right.type === ODataFilterType.LITERAL) {
      ret.right = (filter.right as ODataFilterTypeLiteral).value;
    } else if (filter.right.type === ODataFilterType.PROPERTY) {
      ret.right = (filter.right as ODataFilterTypeProperty).name;
    }
    return ret;
  }

  private generateOrdering(odata: OData): void {
    odata.$orderby.forEach((el) => {
      const keys = Object.keys(el);
      keys.forEach((key) => {
        this.query.orderBy(key, el[key]);
      });
    });
  }

  private generateExpand(odata: OData): void {
    this.expand = odata.$expand;
  }

  insertData(element: any): void {
    this.query = this.knex.table(this.table);
    this.query.insert(element);
  }

  updateData(element: any, odata?: OData): void {
    this.query = this.knex.table(this.table);
    if (odata?.$filter) {
      this.generateFilter(odata);
    }
    this.query.update(element);
  }

  deleteData(odata?: OData): void {
    this.query = this.knex.table(this.table);
    if (odata?.$filter) {
      this.generateFilter(odata);
    }
    this.query.delete();
  }

  async count(): Promise<number> {
    const queryCount = this.query.clone().count('*', { as: 'count' }).first();
    this.logger.debug(
      `[${queryCount.toSQL().sql}] -> [${queryCount.toSQL().bindings}]`,
    );
    return (await queryCount).count;
  }

  async exec(skip?: number, top?: number, relations?: boolean): Promise<any[]> {
    if (skip != undefined) {
      this.query.offset(skip);
    }
    if (top != undefined) {
      this.query.limit(top);
    }

    this.logger.debug(
      `[${this.query.toSQL().sql}] -> [${this.query.toSQL().bindings}]`,
    );

    const data = (await this.query) as any[];

    if (relations) {
      await this.join(data);
    }

    return data;
  }

  private async join(data: any[]): Promise<void> {
    const foreignKeys: DBScanTableRelation[] = this.scanner.getTableRelations(
      this.table,
    );

    let filteredFk: DBScanTableRelation[] = [];
    if (this.expand.length > 0) {
      filteredFk = foreignKeys.filter((fk) => {
        return this.expand.some(
          (expandable) =>
            expandable.toLowerCase() === fk.refTable.toLowerCase(),
        );
      });
    }

    const foreachData = data.map<Promise<void>>(async (row) => {
      const foreachKey = filteredFk.map<Promise<void>>(async (fk) => {
        const joinQuery = this.knex
          .table(fk.refTable)
          .where(`${fk.refTable}.${fk.refKey}`, `${row[fk.foreignKey]}`);
        this.logger.debug(
          `[${joinQuery.toSQL().sql}] -> [${joinQuery.toSQL().bindings}]`,
        );
        row[fk.refTable] = await joinQuery;
      });
      await Promise.all(foreachKey);
    });

    await Promise.all(foreachData);
  }
}
