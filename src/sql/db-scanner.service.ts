/**
 * Copyright (C) 2021  Raffaele Francesco Mancino raffaelefrancesco.mancino@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 **/
import { ConsoleLogger, Injectable } from '@nestjs/common';
import { InjectKnex, Knex } from 'nestjs-knex';
import { SchemaInspector } from 'knex-schema-inspector/dist/types/schema-inspector';
import schemaInspector from 'knex-schema-inspector';
import { DBScan, DBScanTable, DBScanTableRelation } from '../models';

@Injectable()
export class DbScannerService {
  private inspector: SchemaInspector;
  private schema: DBScan = {
    tables: [],
  };

  constructor(
    @InjectKnex() private readonly knex: Knex,
    private logger: ConsoleLogger,
  ) {
    this.inspector = schemaInspector(this.knex);
    this.logger.log('DB Scan started');
    this.scan().then(
      () => {
        this.logger.log('DB Scan finished');
      },
      (err) => {
        this.logger.error('DB Scan error', err);
      },
    );
  }

  private async scan(): Promise<void> {
    const tables = await this.inspector.tables();
    if (tables && tables.length > 0) {
      this.schema.tables = tables.map<DBScanTable>((name) => {
        const tbl: DBScanTable = {
          pk: '',
          name,
          relations: [],
        };
        return tbl;
      });

      const pkScan = this.schema.tables.map<Promise<void>>(async (table) => {
        table.pk = await this.inspector.primary(table.name);
      });
      await Promise.all(pkScan);

      const fkScan = await this.inspector.foreignKeys();
      fkScan.forEach((fk) => {
        const table = this.schema.tables.find((table) => {
          return table.name === fk.table;
        });
        if (table) {
          table.relations.push({
            foreignKey: fk.column,
            refTable: fk.foreign_key_table,
            refKey: fk.foreign_key_column,
          });
        }

        const refTable = this.schema.tables.find(
          (table) => table.name === fk.foreign_key_table,
        );
        if (refTable) {
          refTable.relations.push({
            foreignKey: fk.foreign_key_column,
            refTable: fk.table,
            refKey: fk.column,
          });
        }
      });
    }
  }

  getTableRelations(tableName: string): DBScanTableRelation[] {
    const table = this.schema.tables.find((table) => table.name === tableName);
    if (table) {
      const clone = JSON.parse(
        JSON.stringify(table.relations),
      ) as DBScanTableRelation[];
      return clone;
    }
    return [];
  }

  getTablePk(tableName: string): string {
    return (
      this.schema.tables.find((table) => table.name === tableName)?.pk ?? ''
    );
  }

  tableExist(tableName: string): boolean {
    return this.schema.tables.some((table) => table.name === tableName);
  }
}
