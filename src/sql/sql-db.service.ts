/**
 * Copyright (C) 2021  Raffaele Francesco Mancino raffaelefrancesco.mancino@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 **/
import { ConsoleLogger, Injectable } from '@nestjs/common';

import { InjectKnex, Knex } from 'nestjs-knex';

import { OData, ODataFilterType, TrendkillResponseDTO } from '../models';
import { TrendkillQueryBuilder } from '../utils';
import { DbScannerService } from './db-scanner.service';
import { DBApiServiceInterface } from 'src/api/db-api.service';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const parser = require('odata-parser');

@Injectable()
export class SqlDbService implements DBApiServiceInterface {
  constructor(
    @InjectKnex() private readonly knex: Knex,
    private logger: ConsoleLogger,
    private dbScanner: DbScannerService,
  ) {}

  async execQuery(
    table: string,
    httpQuery?: string,
  ): Promise<TrendkillResponseDTO> {
    if (!this.dbScanner.tableExist(table)) {
      throw new Error(`Table ${table} doesn't exist`);
    }

    try {
      let odataQuery: OData = {};
      if (httpQuery) {
        odataQuery = parser.parse(httpQuery) as OData;
      }

      const queryBuilder: TrendkillQueryBuilder = new TrendkillQueryBuilder(
        table,
        this.knex,
        this.logger,
        this.dbScanner,
      );
      queryBuilder.getData(odataQuery);

      const total = await queryBuilder.count();
      const data = await queryBuilder.exec(
        odataQuery.$skip,
        odataQuery.$top,
        true,
      );

      return {
        data,
        total,
      };
    } catch (e) {
      throw new Error(e);
    }
  }

  async addElement(table: string, element: any): Promise<TrendkillResponseDTO> {
    if (!this.dbScanner.tableExist(table)) {
      throw new Error(`Table ${table} doesn't exist`);
    }

    try {
      const queryBuilder: TrendkillQueryBuilder = new TrendkillQueryBuilder(
        table,
        this.knex,
        this.logger,
        this.dbScanner,
      );
      queryBuilder.insertData(element);

      const data = await queryBuilder.exec();
      return {
        data,
        total: data.length,
      };
    } catch (e) {
      throw new Error(e);
    }
  }

  async updateElement(
    table: string,
    pk: string,
    element: any,
  ): Promise<TrendkillResponseDTO> {
    if (!this.dbScanner.tableExist(table)) {
      throw new Error(`Table ${table} doesn't exist`);
    }

    try {
      const queryBuilder: TrendkillQueryBuilder = new TrendkillQueryBuilder(
        table,
        this.knex,
        this.logger,
        this.dbScanner,
      );
      const pkName = this.dbScanner.getTablePk(table);
      if (pkName) {
        queryBuilder.updateData(element, {
          $filter: {
            type: ODataFilterType.EQ,
            left: {
              type: ODataFilterType.PROPERTY,
              name: pkName,
            },
            right: {
              type: ODataFilterType.LITERAL,
              value: pk,
            },
          },
        });

        const data = await queryBuilder.exec();
        return {
          data,
          total: data.length,
        };
      }
    } catch (e) {
      throw new Error(e);
    }
  }

  async deleteElement(
    table: string,
    pk: string,
  ): Promise<TrendkillResponseDTO> {
    if (!this.dbScanner.tableExist(table)) {
      throw new Error(`Table ${table} doesn't exist`);
    }

    try {
      const queryBuilder: TrendkillQueryBuilder = new TrendkillQueryBuilder(
        table,
        this.knex,
        this.logger,
        this.dbScanner,
      );
      const pkName = this.dbScanner.getTablePk(table);
      if (pkName) {
        queryBuilder.deleteData({
          $filter: {
            type: ODataFilterType.EQ,
            left: {
              type: ODataFilterType.PROPERTY,
              name: pkName,
            },
            right: {
              type: ODataFilterType.LITERAL,
              value: pk,
            },
          },
        });

        const data = await queryBuilder.exec();
        return {
          data,
          total: data.length,
        };
      }
    } catch (e) {
      throw new Error(e);
    }
  }
}
