import { ConsoleLogger, Module } from '@nestjs/common';
import { SqlDbService } from './sql-db.service';
import { DbScannerService } from './db-scanner.service';

@Module({
  providers: [
    {
      provide: 'DBApiServiceInterface',
      useExisting: SqlDbService
    },
    SqlDbService,
    DbScannerService,
    ConsoleLogger
  ],
  exports: [
    'DBApiServiceInterface'
  ]
})
export class SqlModule {}
