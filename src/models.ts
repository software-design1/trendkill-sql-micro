/**
 * Copyright (C) 2021  Raffaele Francesco Mancino raffaelefrancesco.mancino@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 **/

// OData
export interface OData {
  $select?: string[];
  $filter?: ODataFilter;
  $orderby?: any[];
  $skip?: number;
  $top?: number;
  $expand?: string[];
}

export interface ODataFilter {
  type: ODataFilterType;
  left: ODataFilterTypeLiteral | ODataFilterTypeProperty | ODataFilter;
  right: ODataFilterTypeLiteral | ODataFilterTypeProperty | ODataFilter;
}

export interface ODataFilterTypeLiteral {
  type: ODataFilterType.LITERAL;
  value: string;
}

export interface ODataFilterTypeProperty {
  type: ODataFilterType.PROPERTY;
  name: string;
}

export enum ODataFilterType {
  LITERAL = 'literal',
  PROPERTY = 'property',

  EQ = 'eq',
  NOT_EQ = 'ne',
  GREATER = 'gt',
  GR_EQ = 'ge',
  LESS = 'lt',
  LE_EQ = 'le',
  HAS = 'has',
  IS = 'in',

  AND = 'and',
  OR = 'or',
}

// Business
export interface QueryParams {
  left: string;
  right: string;
}

export interface TrendkillResponseDTO {
  data: any[];
  total: number;
}

// Scanner
export interface DBScan {
  tables: DBScanTable[];
}

export interface DBScanTable {
  pk: string;
  name: string;
  relations: DBScanTableRelation[];
}

export interface DBScanTableRelation {
  foreignKey: string;
  refTable: string;
  refKey: string;
}
