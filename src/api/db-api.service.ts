import { TrendkillResponseDTO } from 'src/models';

export interface DBApiServiceInterface {
  execQuery(table: string, httpQuery?: string): Promise<TrendkillResponseDTO>;
  addElement(table: string, element: any): Promise<TrendkillResponseDTO>;
  updateElement(table: string, pk: string, element: any);
  deleteElement(table: string, pk: string): Promise<TrendkillResponseDTO>;
}
