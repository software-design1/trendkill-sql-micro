/**
 * Copyright (C) 2021  Raffaele Francesco Mancino raffaelefrancesco.mancino@gmail.com
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.
 **/
import {
  BadRequestException,
  Body,
  ConsoleLogger,
  Controller,
  Delete,
  Get,
  ImATeapotException,
  Inject,
  Param,
  Post,
  Put,
  Req,
} from '@nestjs/common';
import { ApiOperation, ApiTags } from '@nestjs/swagger';
import { DBApiServiceInterface } from './api/db-api.service';
import { TrendkillResponseDTO } from './models';

@ApiTags('Query Path')
@Controller()
export class AppController {
  constructor(@Inject('DBApiServiceInterface') private queryService: DBApiServiceInterface, private logger: ConsoleLogger) {}

  @Get(':table')
  @ApiOperation({
    summary: 'Get rows',
  })
  async getData(
    @Param('table') table: string,
    @Req() req,
  ): Promise<TrendkillResponseDTO> {
    try {
      const httpQueryString = decodeURI(req.url).split('?');
      if (httpQueryString.length == 2) {
        return await this.queryService.execQuery(table, httpQueryString[1]);
      } else {
        return await this.queryService.execQuery(table);
      }
    } catch (e) {
      this.logger.error(e);
      throw new ImATeapotException();
    }
  }

  @Post(':table')
  @ApiOperation({
    summary: 'Insert row',
  })
  async addData(
    @Param('table') table: string,
    @Body() body,
  ): Promise<TrendkillResponseDTO> {
    if (body == undefined) {
      throw new BadRequestException();
    }

    try {
      return this.queryService.addElement(table, body);
    } catch (e) {
      this.logger.error(e);
      throw new ImATeapotException();
    }
  }

  @Put(':table/:pk')
  @ApiOperation({ summary: 'Alter row' })
  async alterData(
    @Param('table') table: string,
    @Param('pk') pk: string,
    @Body() body,
  ): Promise<TrendkillResponseDTO> {
    if (body == undefined) {
      throw new BadRequestException();
    }
    try {
      return await this.queryService.updateElement(table, pk, body);
    } catch (e) {
      this.logger.error(e);
      throw new ImATeapotException();
    }
  }

  @Delete(':table/:pk')
  @ApiOperation({ summary: 'Delete row' })
  async deleteData(
    @Param('table') table: string,
    @Param('pk') pk: string,
  ): Promise<TrendkillResponseDTO> {
    try {
      return await this.queryService.deleteElement(table, pk);
    } catch (e) {
      this.logger.error(e);
      throw new ImATeapotException();
    }
  }
}
