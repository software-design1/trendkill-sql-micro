FROM node:14.17.5-alpine

WORKDIR /usr/src/app

COPY package*.json ./
COPY dist ./

RUN npm install

CMD ["node", "main"]
